"""Second Dash app."""
# Import required libraries
import os
from random import randint

import dash
import flask
import pandas as pd
import plotly.express as px
from dash import Input, Output, dcc, html

# Setup the app
# Make sure not to change this file name or the variable names below,
# the template is configured to execute 'server' on 'app.py'
server = flask.Flask(__name__)
server.secret_key = os.environ.get("secret_key", str(randint(0, 1000000)))
app = dash.Dash(__name__, server=server)


# TODO: Change the PATH to dataset in pd.read_csv()
# Check the `datasets` folder.
# Possible choices:
# 1. `datasets/gross_value_added.csv`
# 2. `datasets/balance_of_payments.csv`

# TODO: Replace "Indicator Name" with "Series"

df = pd.read_csv("datasets/country_indicators.csv", engine="python")

app.layout = html.Div(
    [
        html.H1(
            children="Interactive Dash App",
            style={
                "textAlign": "center",
            },
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Dropdown(
                            df["Series"].unique(),
                            df["Series"].unique()[0],
                            id="xaxis-column",
                        ),
                    ],
                    style={"width": "48%", "display": "inline-block"},
                ),
                html.Div(
                    [
                        dcc.Dropdown(
                            df["Series"].unique(),
                            df["Series"].unique()[1],
                            id="yaxis-column",
                        ),
                    ],
                    style={
                        "width": "48%",
                        "float": "right",
                        "display": "inline-block",
                    },
                ),
            ]
        ),
        dcc.Graph(id="indicator-graphic"),
        dcc.Slider(
            df["Year"].min(),
            df["Year"].max(),
            step=None,
            id="year--slider",
            value=df["Year"].max(),
            marks={str(year): str(year) for year in df["Year"].unique()},
        ),
        dcc.Div("UN Data - Balance of Payments Summary"),
    ]
)


@app.callback(
    Output("indicator-graphic", "figure"),
    Input("xaxis-column", "value"),
    Input("yaxis-column", "value"),
    Input("year--slider", "value"),
)
def update_graph(
    xaxis_column_name: str, yaxis_column_name: str, year_value: str
) -> object:
    """Update the figure of the plotly based on the chosen column values.

    Parameters:
    ----------
    `xaxis_column_name`: str
        Drop down value chosen as the x-axis column name.
    `yaxis_column_name`: str
        Drop down value chosen as the y-axis column name.
    `year_value`: int
        Year chosen from the slider.

    Returns:
    --------
    object:
        `figure` object of plotly.

    """
    dff = df[df["Year"] == year_value]

    x_values = dff[dff["Series"] == xaxis_column_name]["Value"]
    y_values = dff[dff["Series"] == yaxis_column_name]["Value"]
    hover_values = dff[dff["Series"] == yaxis_column_name][
        "Country Name"
    ]

    min_x_y = len(x_values)
    if len(x_values) != len(y_values):
        min_x_y = min(len(x_values), len(y_values))

    fig = px.scatter(
        x=x_values.head(min_x_y),
        y=y_values.head(min_x_y),
        hover_name=hover_values.head(min_x_y),
    )

    fig.update_xaxes(title=xaxis_column_name)
    fig.update_yaxes(title=yaxis_column_name)
    fig.update_layout(transition_duration=1000)

    return fig


# Run the Dash app
if __name__ == "__main__":
    app.server.run(debug=True, threaded=True)
